import { beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';
import $ from 'jquery';
import LoadingView from '../src/loading-view';

let sut;

describe('loading-view', () => {
    beforeEach(() => {
        document.body.innerHTML = `
            <div class="container"></div>
        `;
        sut = new LoadingView({
            $container: $('.container'),
            offset: {},
        });
    });

    it('renders', () => {
        expect(sut.$el.text()).to.equal('Loading...');
    });

    it('can hide', () => {
        sut.hide();
        expect(sut.el.parentNode).to.equal(null);
    });

    it('can show', () => {
        sut.show();
        expect($('.container').find('.loading').length).to.equal(1);
    });

});
